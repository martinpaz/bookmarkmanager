# Bookmark Manager

---
https://bitbucket.org/martinpaz/bookmarkmanager/
---

## Screenshots

#### Overview
![Alt text](./src/assets/overview.PNG?raw=true "Overview")
#### Add Dialog
![Alt text](./src/assets/add-dialog.PNG?raw=true "Add Dialog")
#### Edit Dialog
![Alt text](./src/assets/edit-dialog.PNG?raw=true "Edit Dialog")
#### Tooltips
![Alt text](./src/assets/add-button.png?raw=true "Tooltip Add")
![Alt text](./src/assets/edit-button.png?raw=true "Tooltip Edit")
![Alt text](./src/assets/edit-button.png?raw=true "Tooltip Delete")

## Prerequisites

NPM

6.13.7

Node.js

v13.9.0


## Run the application

- Go to the root folder (bookmark\src\app) and run: 
```sh
npm install
npm start
```
 - App will automatically launch at http://localhost/80.

## Functionalities

1. Add bookmarks.
1. Edit bookmark
1. Delete bookmark
1. Specify an existing or new group for a bookmark.
1. View all bookmarks
 
---
## Description of the solution

### Folder structure & files

- **src/app/store/**: contains all the components that implement Redux inspired NgRx pattern for managing the state of the bookmark.

- **src/app/store/app.state.ts**: contains the state (store) of the whole application

- **src/app/services/bookmark.service.ts**: contains a mock service that returns some predefined bookmarks.

### NgRx Actions
- **GET_BOOKMARKS** = '[Bookmark] Get Bookmarks',
- **ADD_BOOKMARK**= '[Bookmark] Add Bookmark',
- **UPDATE_BOOKMARK**= '[Bookmark] Update Bookmark',
- **DELETE_BOOKMARK** = '[Bookmark] Delete Bookmark',
- **LOAD_BOOKMARK_INIT** = '[Bookmark] Load Bookmark init',
- **LOAD_BOOKMARK_DONE** = '[Bookmark] Load Bookmark done',
- **RESTORE_BOOKMARKS** = '[Bookmark] Restore Bookmarks to current state'

### NgRx Reducers
The reducer "listens" to the following actions, apart from the default one:

- **GET_BOOKMARKS**: returns the state with all bookmarks
- **ADD_BOOKMARK**: adds the new bookmark to the state and returns the updated state
- **UPDATE_BOOKMARK**: replaces the edited bookmark to the state and returns the updated state
- **LOAD_BOOKMARK_DONE**:  adds the new bookmarks that were created by **bookmark.service.ts -> getBookmarks()** to the state and returns the updated state
- **RESTORE_BOOKMARKS**: it is not handled by the reducer, so the default case is executed. It was used in order to return the existing state after the action call, if needed.

### NgRx Effects

- **loadBookmarks**: Triggered by **LOAD_BOOKMARK_INIT** actions and calls **LOAD_BOOKMARK_DONE**.
> As there is no error handling, the postfix _DONE was used instead of _SUCCESS for the LOAD_BOOKMARK_DONE action

### NgRx Selectors 
- **selectBookmarks**: returns an observable with all bookmarks
- **selectGroup**: returns an observable with all the groups & **'All Bookmarks'** group added by default
- **selectBookmarksByGroup**: returns an observable with the bookmarks of the group that was passed as a parameter.

### NgRx States
- **interfaces**: Bookmark, Bookmarks
- **initialStates**: initialBookmarksState

## Initial data
As a showcase initial data are loaded as follows:
- by calling **LOAD_BOOKMARK_INIT** action in **src\app\store\bookmark.actions.ts** -> ngOnInit(). That action triggers an effect and the **bookmark manager service** (relevant Spring Boot Project on 8900 port) is called.
> That is the only case that an effect is being called. In all other cases state modified directly in the reducer.