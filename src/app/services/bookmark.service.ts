import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bookmarks } from '../store/bookmark.state';

@Injectable({providedIn: 'root'})
export class BookmarkService {

    private dummyBookmarks: Bookmarks = {
        bookmarks: [
            {id:1, name: 'GMail', url: 'mail.google.com', group: 'personal'},
            {id:2, name: 'Facebook', url: 'facebook.com', group: 'leisure'},
            {id:3, name: 'Youtube', url: 'youtube.com', group: 'social'},
            {id:4, name: 'LinkedIn', url: 'linkedin.com', group: 'work'},
            {id:5, name: 'Amazon', url: 'amazon.com', group: 'shopping'}
        ]
    };

    getBookmarks(): Observable<Bookmarks>{
        return new Observable(observer => {
            observer.next(this.dummyBookmarks);
            observer.complete();
        });
    }
}
