import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { initialState, reducers, effects } from './store/app.state';

import { GroupComponent } from './components/group/group.component';
import { BaseButtonComponent } from './components/base-button/base-button.component';
import { MessageComponent } from './components/base-button/base-button.component';
import { BaseTableComponent } from './components/base-table/base-table.component';
import { AddGroupComponent } from './components/add-group/add-group.component';
import { UpdateGroupComponent } from './components/update-group/update-group.component';


import { BookmarkService } from './services/bookmark.service';
import { NotificationService } from './services/notification.service';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
	    MatButtonModule,
	    MatDialogModule,
	    MatIconModule,
	    MatInputModule,
	    MatListModule,
	    MatSidenavModule,
	    MatSnackBarModule,
	    MatTableModule,
	    MatToolbarModule,
	    MatTooltipModule,
        FormsModule,
        EffectsModule.forRoot(effects),
        StoreModule.forRoot(reducers, {initialState}),
        StoreDevtoolsModule.instrument( {maxAge: 30} ),
    ],
    declarations: [
        AppComponent,
		GroupComponent,
        BaseButtonComponent,
        MessageComponent,
        BaseTableComponent,
		AddGroupComponent,
		UpdateGroupComponent
    ],
    providers: [
	    BookmarkService, NotificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }