import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.state';
import { NotificationService } from '../../services/notification.service';

interface SimpleTableConfig {
	columns: any[]
	displayedColumns: string[]
    tooltip: any
    data: any
    editButtonConfig: any
    deleteButtonConfig: any
    deleteFunc: Function
}

@Component({
	selector: 'base-table',
    styleUrls: ['base-table.component.scss'],
    templateUrl: 'base-table.component.html',
})
export class BaseTableComponent {
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    @Input() config : SimpleTableConfig;

	delete(data: any) {
		this.config.deleteFunc(data, this.store, this.notificationService);
	}
}
