import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AddGroupComponent } from '../add-group/add-group.component';
import { UpdateGroupComponent } from '../update-group/update-group.component';
import { AppState, selectBookmarks, selectGroup, selectBookmarksByGroup} from '../../store/app.state'; 
import { Bookmark } from '../../store/bookmark.state';
import { LoadBookmarkInit, DeleteBookmark } from '../../store/bookmark.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'group',
    styleUrls: ['group.component.scss'],
    templateUrl: 'group.component.html',
})
export class GroupComponent {
    groupTableConfig;
    addButtonConfig;
    activeFolder;
    groups$: Observable<string[]>;
    groups: string[];
    bookmarks$: Observable<Bookmark[]>;
    bookmarks: Bookmark[];
    selectedGroup: string;

    constructor(private store: Store<AppState>) {

    }

	ngOnInit() {
	    this.addButtonConfig = {
			type: 'template',
			attribute: 'mat-mini-fab',
			fontIconColor: 'white',
			fontIcon:'add',
			tooltip:'Add bookmark',
			template: AddGroupComponent
		}

		this.loadData();
		
		this.groupTableConfig = {
			data: this.bookmarks,
			displayedColumns: ['name', 'url', 'group', 'action'],
			columns: [{
				header: "Site",
				field: "name"
			},{
				header: "URL",
				field: "url"
			},{
				header: "Group",
				field: "group"
			}],
			editButtonConfig: {
				type: 'template',
				attribute: 'mat-icon-button',
				fontIconColor: 'primary',
				fontIcon:'edit',
				tooltip:'Edit bookmark',
				template: UpdateGroupComponent
			},
			deleteButtonConfig: {
				type: 'message',
				attribute: 'mat-icon-button',
				fontIconColor: 'warn',
				fontIcon:'delete',
				tooltip:'Delete bookmark',
				messageConfig: {
				    message: 'Are you sure you want to remove this bookmark ?',
                    primaryButtonEnabled: true,
                    primaryButtonLabel: 'Yes',
                    secondaryButtonEnabled: true,
                    secondaryButtonLabel: 'No',
                    primaryButtonCallback: function(store: Store<AppState>, notificationService: NotificationService, data: any) {
	                    store.dispatch(new DeleteBookmark(data));
					    notificationService.showNotification({
						    duration: 2000,
						    vPos: 'top',
						    hPos: 'center',
						    message: 'Bookmark was deleted successfully.'
					    });
                    }
				}
			}
		}
	}

	isActive(group: string) {
	    return group === this.activeFolder;
	}

    private loadData() {
	    this.groups$ = this.store.pipe(select(selectGroup));
        this.groups$.subscribe(response => {
		    this.groups=response;
        });

        this.loadGroup('All Bookmarks');
        this.activeFolder='All Bookmarks';
        this.loadBookmarks();
    }

    loadGroup(group : string) {
	    this.selectedGroup = group;
        if(group === 'All Bookmarks') {
	        this.bookmarks$ = this.store.pipe(select(selectBookmarks));
        } else {
	        this.bookmarks$ = this.store.pipe(select(selectBookmarksByGroup(group)));
        }

        this.bookmarks$.subscribe(response => {
		    this.bookmarks=response;

			if(this.groupTableConfig) {
				this.groupTableConfig.data=this.bookmarks;
			}
        });

        this.activeFolder = group;
    }

    private loadBookmarks() {
	    this.store.dispatch(new LoadBookmarkInit(null));
    }
}
