import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.state';
import { Bookmark } from '../../store/bookmark.state';
import { UpdateBookmark } from '../../store/bookmark.actions';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../../services/notification.service';

@Component({
    styleUrls: ['update-group.component.scss'],
    templateUrl: 'update-group.component.html',
})
export class UpdateGroupComponent {
	@ViewChild('updateCloseButton', null) updateCloseButton;
	
    constructor(private store: Store<AppState>, @Inject(MAT_DIALOG_DATA) public data: any, private notificationService: NotificationService) {
        
    }

    name: FormControl;
	url: FormControl;
	group: FormControl;
	bookmark : Bookmark;

    ngOnInit() {
	    this.name = new FormControl(this.data.name, [
	        Validators.required
	    ]);

	    this.url = new FormControl(this.data.url, [
	        Validators.required
	    ]);

        this.group = new FormControl(this.data.group, [
	        Validators.required,
            Validators.maxLength(15)
	    ]);
	}

	update() {
	    if(this.name.hasError('required')) {
		    return false;
	    }

		if(this.url.hasError('required')) {
		    return false;
	    }

		if(this.group.hasError('required') || this.group.hasError('maxlength')) {
		    return false;
	    }

		let name = this.name.value;
		let url = this.url.value;
		let group = this.group.value;

	    this.bookmark = {
		    id: this.data.id, name, url, group
	    }

		this.store.dispatch(new UpdateBookmark(this.bookmark));

		this.updateCloseButton._elementRef.nativeElement.click();
		
		this.notificationService.showNotification({
			duration: 2000,
			vPos: 'top',
			hPos: 'center',
			message: 'Group was updated successfully!'
		});
    }
}
