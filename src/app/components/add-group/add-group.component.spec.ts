import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AddGroupComponent } from './add-group.component';
import { NotificationService } from '../../services/notification.service';
import { Store } from '@ngrx/store';

describe('AddGroupComponent', () => {
	let component: AddGroupComponent;
	let fixture: ComponentFixture<AddGroupComponent>;
    let notificationServiceSpy: jasmine.SpyObj<NotificationService>;
	let spyBookmark: jasmine.SpyObj<Store>;
	
	beforeEach(() => {
		const spy = jasmine.createSpyObj('NotificationService', ['showNotification']);
		const mockBookmark = jasmine.createSpyObj('Store', ['bookmark']);
		mockBookmark.bookmark(({
			name: 'site a',
			url: 'a.com',
			group: 'a'
		}));
		
		TestBed.configureTestingModule({
			providers: [
				{ provide: NotificationService, useValue: spy },
				{ provide: Store, useValue: mockBookmark }
			]
		});

		notificationServiceSpy = TestBed.inject(NotificationService) as jasmine.SpyObj<NotificationService>;
		spyBookmark = TestBed.inject(Store) as jasmine.SpyObj<Store>;
	});   

	beforeEach(() => {
		fixture = TestBed.createComponent(AddGroupComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create new', () => {
		expect(component).toBeTruthy();
	});
});
