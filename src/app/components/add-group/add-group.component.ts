import {Component, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.state';
import { Bookmark } from '../../store/bookmark.state';
import { AddBookmark } from '../../store/bookmark.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    styleUrls: ['add-group.component.scss'],
    templateUrl: 'add-group.component.html',
})
export class AddGroupComponent {
	@ViewChild('addCloseButton', null) addCloseButton;
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    name: FormControl;
	url: FormControl;
	group: FormControl;
	bookmark : Bookmark;

    ngOnInit() {
	    this.name = new FormControl('', [
	        Validators.required
	    ]);

	    this.url = new FormControl('', [
	        Validators.required
	    ]);

	    this.group = new FormControl('', [
	        Validators.required,
            Validators.maxLength(15)
	    ]);
	}

    add() {
	    if(this.name.hasError('required')) {
		    return false;
	    }

		if(this.url.hasError('required')) {
		    return false;
	    }

		if(this.group.hasError('required') || this.group.hasError('maxlength')) {
		    return false;
	    }

		let name = this.name.value;
		let url = this.url.value;
		let group = this.group.value;

	    this.bookmark = {
		    id: new Date().getTime(), name, url, group
	    }

		this.store.dispatch(new AddBookmark(this.bookmark));
		
		this.addCloseButton._elementRef.nativeElement.click();
		
		this.notificationService.showNotification({
			duration: 2000,
			vPos: 'top',
			hPos: 'center',
			message: 'Group was added successfully!'
		});
    }
}
