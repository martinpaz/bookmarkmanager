export interface Bookmark {
	id: number,
	name: string,
	url: string,
	group: string
}

export interface Bookmarks {
	bookmarks: Bookmark[];
}

export const initialBookmarksState: Bookmarks = {
	bookmarks: []
}