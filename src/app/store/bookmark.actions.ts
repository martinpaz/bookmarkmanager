import { Action } from '@ngrx/store';

import { Bookmark, Bookmarks } from '../store/bookmark.state';

export enum EBookmarkActions {
    GET_BOOKMARKS = '[Bookmark] Get Bookmarks',
    ADD_BOOKMARK = '[Bookmark] Add Bookmark',
    UPDATE_BOOKMARK = '[Bookmark] Update Bookmark',
    DELETE_BOOKMARK = '[Bookmark] Delete Bookmark',
    LOAD_BOOKMARK_INIT = '[Bookmark] Load Bookmark init',
    LOAD_BOOKMARK_DONE = '[Bookmark] Load Bookmark done',
    RESTORE_BOOKMARKS = '[Bookmark] Restore Bookmarks to current state'
}

export class GetBookmarks implements Action {
    readonly type = EBookmarkActions.GET_BOOKMARKS;

    constructor(public payload: any) {
        console.log('ACTION ' + EBookmarkActions.GET_BOOKMARKS);
    }
}

export class AddBookmark implements Action {
    readonly type = EBookmarkActions.ADD_BOOKMARK;

    constructor(public payload: Bookmark) {
        console.log('ACTION ' + EBookmarkActions.ADD_BOOKMARK);
    }
}

export class UpdateBookmark implements Action {
  readonly type = EBookmarkActions.UPDATE_BOOKMARK;

  constructor(public payload: Bookmark) {
      console.log('ACTION ' + EBookmarkActions.UPDATE_BOOKMARK);
  }
}

export class DeleteBookmark implements Action {
    readonly type = EBookmarkActions.DELETE_BOOKMARK;

    constructor(public payload: Bookmark) {
        console.log('ACTION ' + EBookmarkActions.DELETE_BOOKMARK);
    }
}

export class LoadBookmarkInit implements Action {
    readonly type = EBookmarkActions.LOAD_BOOKMARK_INIT;

    constructor(public payload: any){
        console.log('ACTION ' + EBookmarkActions.LOAD_BOOKMARK_INIT);
    }
}

export class LoadBookmarkDone implements Action {
    readonly type = EBookmarkActions.LOAD_BOOKMARK_DONE;

    constructor(public payload: Bookmarks) {
        console.log('Bookmarks - ' + payload);
    }
}

export class RestoreBookmarks implements Action {
  readonly type = EBookmarkActions.RESTORE_BOOKMARKS;

  constructor(public payload: any) {
      console.log('ACTION ' + EBookmarkActions.RESTORE_BOOKMARKS);
  }
}

export type BookmarkActions = GetBookmarks | AddBookmark | UpdateBookmark | DeleteBookmark | LoadBookmarkInit | LoadBookmarkDone | RestoreBookmarks;
