import { EBookmarkActions, BookmarkActions } from '../store/bookmark.actions';
import { Bookmarks, initialBookmarksState } from '../store/bookmark.state';

export function bookmarkReducer(state = initialBookmarksState, action: BookmarkActions): Bookmarks {
	switch (action.type) {
        case EBookmarkActions.GET_BOOKMARKS: {
            return  {...state};
        }
        case EBookmarkActions.ADD_BOOKMARK: {
            return  { bookmarks: sort(state.bookmarks.concat(action.payload), 'name')};
        }
        case EBookmarkActions.UPDATE_BOOKMARK: {
	        return  { bookmarks: sort(state.bookmarks.filter(bookmark => bookmark.id !== action.payload.id).concat(action.payload), 'name')};
        }
        case EBookmarkActions.DELETE_BOOKMARK: {
            return  { bookmarks: sort(state.bookmarks.filter(bookmark => bookmark.id !== action.payload.id), 'name')};
        }
        case EBookmarkActions.LOAD_BOOKMARK_DONE: {
            return  { bookmarks: sort(state.bookmarks.concat(action.payload.bookmarks), 'name')};
        }
        default: {
            return {
                ...state
            };
        }
	}
}

function sort(bookmarks, property) {
	return bookmarks.sort((a,b) => {
        if(a[property] < b[property]) {
            return -1;
        } else {
            return (a[property] > b[property]) ? 1 : 0
        }
    });
}