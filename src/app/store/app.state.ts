import { ActionReducerMap } from '@ngrx/store';
import { createSelector } from '@ngrx/store';
import * as bookmarkState from '../store/bookmark.state';
import { bookmarkReducer } from '../store/bookmark.reducers';
import { BookmarkEffects } from '../store/bookmark.effects';

export interface AppState {
	bookmarks: bookmarkState.Bookmarks;
}

export const initialState: AppState = {
	bookmarks: bookmarkState.initialBookmarksState
};

export const reducers: ActionReducerMap<AppState> = {
	bookmarks: bookmarkReducer
};

export const effects: Array<any> = [
    BookmarkEffects
];

export const selectBookmarks = (appState: AppState) => appState.bookmarks.bookmarks;



export const selectGroup = createSelector(
    selectBookmarks, (bookmarks: bookmarkState.Bookmark[]) => {
        const groups = Object.keys(groupBy(bookmarks, 'group'));
        return ['All Bookmarks'].concat(groups);
    }
);



export const selectBookmarksByGroup = (group: string) => createSelector(
    selectBookmarks, (bookmarks: bookmarkState.Bookmark[]) => {
        return bookmarks.filter(bookmark => bookmark.group === group);
    }
);

function groupBy(objectArray, property) {
    return objectArray.reduce(function (s, obj) {
        const key = obj[property];
        if (!s[key]) {
            s[key] = [];
        }
        s[key].push(obj);
        return s;
    }, {});
}
